<?php
session_start();

$servername = "localhost";
$username = "admin";
$password = "";
$dbname = "tp_guti";

$conn = mysqli_connect($servername, $username, $password, $dbname);

if (!$conn) {
    die("Conexión fallida: " . mysqli_connect_error());
}

$nombre = $_POST["usuario"];
$pass = $_POST["contrasena"];

$query = mysqli_prepare($conn, "SELECT * FROM login WHERE usuario = ?");
mysqli_stmt_bind_param($query, "s", $nombre);
mysqli_stmt_execute($query);
$result = mysqli_stmt_get_result($query);

if ($row = mysqli_fetch_assoc($result)) {
    if (password_verify($pass, $row['contrasenia'])) {
        // Contraseña válida

        // Inicio de sesión exitoso, almacenar solo información necesaria en la sesión
        $_SESSION['usuario'] = $nombre;
        $_SESSION['logged_in'] = true;

        // Redirigir al usuario a la página principal
        header("Location: ../HTML/datos.html");
        exit();
    } else {
        // Contraseña incorrecta
        echo "<script>alert('Usuario o contraseña incorrectos');</script>";
        header("Location: ../HTML/login.html");
        exit();
    }
} else {
    // Usuario no encontrado
    echo "<script>alert('Usuario o contraseña incorrectos');</script>";
    header("Location: ../HTML/login.html");
    exit();
}

mysqli_close($conn);
?>
