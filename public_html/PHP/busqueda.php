<?php
  // Conexión a la base de datos
  $servername = "localhost";
  $username = "admin";
  $password = "";
  $dbname = "tp_guti";

  $conn = mysqli_connect($servername, $username, $password, $dbname);
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  }

  $porServicios = isset($_POST['switch-SD']) ? true : false; // Obtener el valor del checkbox


  if(isset($_POST['descripcion'])){
    $descripcion = $_POST['descripcion'];
    if($porServicios==false){
      $sql = "SELECT * FROM servicios WHERE servicios.descripcion LIKE '%$descripcion%'";
      $result = $conn->query($sql);
    }else{
      $sql1 = "SELECT * FROM servicios WHERE servicios.nombre LIKE '%$descripcion%'";
      $result = $conn->query($sql1);
    }

    if ($result->num_rows > 0) {
      // Almacenar los resultados en un array
      $results_array = array();
      while($row = $result->fetch_assoc()) {
        $results_array[] = array(
          "codigo" => $row["codigo"],
          "nombre" => $row["nombre"],
          "descripcion" => $row["descripcion"],
          "precio_taller" => $row["precio_taller"],
          "precio_on_site" => $row["precio_on_site"],
          "rubro" => $row["rubro"]
      );
      }
     
      // Enviar los resultados como respuesta en formato JSON
      header('Content-Type: application/json');
      echo json_encode($results_array);
    } else {
      echo "No se encontraron resultados para la fecha seleccionada.";
    }
  }

  // Cerrar la conexión a la base de datos
  $conn->close();
?>