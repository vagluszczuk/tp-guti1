<?php
// Decodificar el array de datos
$aux = $_POST[''];

$array = json_decode($aux, true);

require_once('tcpdf/tcpdf.php');

// Crear nuevo objeto PDF
$pdf = new TCPDF('P', 'mm', 'A4', true, 'UTF-8');

// Establecer información del documento
$pdf->SetCreator('Presupuesto');
$pdf->SetAuthor('Presupuesto');
$pdf->SetTitle('Presupuesto');
$pdf->SetSubject('Presupuesto');
$pdf->SetKeywords('Presupuesto');

// Habilitar encabezado y pie de página
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(true);

// Establecer margenes
$pdf->SetMargins(15, 15, 15);

// Agregar una nueva página
$pdf->AddPage();

// Definir el porcentaje deseado
$porcentajeAncho = 50; // 50% del ancho total

// Obtener el ancho total de la página en milímetros
$anchoTotalMM = $pdf->GetPageWidth();

// Calcular el ancho en milímetros
$anchoCeldaMM = ($porcentajeAncho / 100) * $anchoTotalMM;

// Establecer el contenido de la factura aquí

$pdf->SetFont('helvetica', 'B', 14);
$pdf->Cell(0, 10, 'Presupuesto', 0, 1, 'C');

$pdf->SetFont('helvetica', '', 10);
$pdf->Cell($anchoCeldaMM, 10, 'Fecha: '.date('Y-m-d'), 0, 1,'R');

$pdf->Cell(0, 0, "___________________________________________________________________________________________", 0, 1,);
$pdf->Cell(0, 5, "", 0, 1,);

$pdf->SetFont('helvetica', 'B', 10);
$pdf->Cell(30, 8, ' Producto', 0, 0, 'L');
$pdf->Cell(30, 8, 'Precio', 0, 0, 'L');

$total = 0;

foreach ($array as $dato) {

    $producto = $dato['nombre'];
    $precio = $dato['precio'];

    $pdf->SetFont('helvetica', '', 10);
    $pdf->Cell(30, 8, " "."juan", 0, 0, 'L');
    $pdf->Cell(30, 8, "$".$precio, 0, 0, 'L');

    $total = $total + $precio;
}

$pdf->Ln(20);


// Calcular totales

$subtot1 = $total;
$iva = $subtot1 * 0.21;
$totaltotal = $subtot1 + $iva;

$pdf->SetFont('helvetica', 'B', 10);

// Crear tabla de totales
$pdf->SetFont('helvetica', 'B', 10);

$pdf->Cell(30, 8, 'Subtotal', 'T,L', 0, 'L');
$pdf->Cell(30, 8, '$'.$subtot1, 'T,R', 1, 'R');

$pdf->Cell(30, 8, 'IVA (21%)', 'L', 0, 'L');
$pdf->Cell(30, 8, '$'.$iva, 'R', 1, 'R');

$pdf->Cell(60, 8, '', 'R,L', 1,);

$pdf->Cell(30, 8, 'Total', 'L,B', 0, 'L');
$pdf->Cell(30, 8, '$'.$totaltotal, 'R,B', 1, 'R');


$pdf->Output('factura.pdf', 'I'); 
?>