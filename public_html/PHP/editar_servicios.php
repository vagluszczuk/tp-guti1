<?php
// Obtener los datos enviados desde JavaScript
$json_data = file_get_contents("php://input");
    
// Decodificar el JSON en un array asociativo
$data = json_decode($json_data, true);

// Realizar la conexión a la base de datos
$servername = "localhost";
$username = "admin";
$password = "";
$dbname = "tp_guti";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Error al conectar con la base de datos: " . $conn->connect_error);
}

// Recorrer los datos actualizados y ejecutar las consultas para modificar la base de datos
    
    $nombre = $data['Nombre'];
    $codigo = $data['Codigo'];
    $descripcion = $data['Descripcion'];
    $precioTaller = $data['Precio taller'];
    $precioOnSite = $data['Precio on site'];
    $rubro = $data['Rubro'];  

    $sql = "UPDATE servicios SET nombre='$nombre', descripcion='$descripcion', precio_taller='$precioTaller', precio_on_site='$precioOnSite', rubro='$rubro' WHERE codigo='$codigo'";
    
    if ($conn->query($sql) !== TRUE) {
        echo "Error al actualizar los datos: " . $conn->error;
    }


// Cerrar la conexión a la base de datos
$conn->close();
?>

