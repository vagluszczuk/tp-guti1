<?php
$servername = "localhost";
$username = "admin";
$password = "";
$dbname = "tp_guti";

$conn = mysqli_connect($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
// Obtener el término de búsqueda de la URL
$searchTerm = $_GET['q'];
$category = $_GET['category']; // Obtener el parámetro de la categoría

// Construir la consulta SQL con filtrado
$sql = "SELECT * FROM servicios WHERE nombre LIKE '%$searchTerm%'";
if (!empty($category)) {
    $sql .= " AND rubro = '$category'";
}
$result = $conn->query($sql);

// Crear un array con los resultados
$results = array();
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $results[] = $row;
    }
}

// Devolver los resultados en formato JSON
header('Content-Type: application/json');
echo json_encode($results);

$conn->close();
?>
