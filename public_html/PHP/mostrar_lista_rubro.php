<?php
$servername = "localhost";
$username = "admin";
$password = "";
$dbname = "tp_guti";

$conn = mysqli_connect($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Consulta para obtener los datos de la base de datos
$consulta = "SELECT rubros.rubros FROM rubros";
$resultado = mysqli_query($conn, $consulta);

// Crear un array para almacenar los datos
$datos = array();

while ($fila = mysqli_fetch_assoc($resultado)) {
    $rubros = $fila['rubros'];
    $datos[] = array('rubros' => $rubros);
}

// Cerrar la conexión
mysqli_close($conn);

// Enviar los datos en formato JSON
header('Content-Type: application/json');
echo json_encode($datos);
?>



