<?php
session_start();

// Comprueba si la cookie de sesión ha expirado
if (isset($_SESSION['expire']) && $_SESSION['expire'] < time()) {
    // La sesión ha expirado, destruye la sesión y redirige al formulario de inicio de sesión
    session_destroy();
    header("Location: ../HTML/login.html");
    exit();
}

// Comprueba si la sesión existe
if (isset($_SESSION['logged_in']) && $_SESSION['logged_in']) {
    // La sesión existe, puedes realizar las operaciones que necesites
    echo "La sesión existe";

    // Actualiza el tiempo de expiración de la sesión
    $_SESSION['expire'] = time() + 1140;
} else {
    // La sesión no existe, redirige al usuario al formulario de inicio de sesión
    header("Location: ../HTML/login.html");
    exit();
}
?>
