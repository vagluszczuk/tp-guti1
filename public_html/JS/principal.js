let compra;

fetch('https://www.dolarsi.com/api/api.php?type=dolar')
  .then(response => response.json())
  .then(data => {
    const blueObj = data.find(item => item.casa.nombre === "Blue");
    compra = blueObj.casa.compra;
    compra = compra.replace(",", ".");
  })
  .catch(error => {
    console.error('Error al obtener el archivo JSON:', error);
  });


var chk_mostrar = 1;

// Agregar un eventListener al checkbox
checkbox.addEventListener("change", function() {
  if (this.checked) {
    chk_mostrar = compra;
  } else {
    chk_mostrar = 1;
  }
  performSearch();
});

// Función para obtener los datos de la base de datos y llenar el desplegable
function llenarDesplegable() {
  fetch('../PHP/mostrar_lista_rubro.php')
    .then(response => response.json())
    .then(data => {
      var desplegable = document.getElementById('rubro');
      data.forEach(dato => {
        var option = document.createElement('option');
        option.value = dato.rubros;
        option.text = dato.rubros;
        desplegable.appendChild(option);
      });
    })
    .catch(error => console.error('Error:', error));
}

// Cargar selecciones al inicio
var selectedRows = [];

// Limpia los checkboxes al cargar la página
window.onload = function() {
  var storedData = localStorage.getItem('myData');
  if (storedData) {
    var parsedData = JSON.parse(storedData);
    selectedRows = parsedData.selectedRows || [];
    chk_mostrar = parsedData.chk_mostrar || 1;

    // Limpia los checkboxes
    selectedRows = [];
    localStorage.removeItem('myData');
  }
  llenarDesplegable();
};

document.getElementById('searchInput').addEventListener('keyup', function() {
  performSearch();
});

document.getElementById('rubro').addEventListener('change', function() {
  performSearch();
});

function performSearch() {
  var searchValue = document.getElementById('searchInput').value;
  var categoryValue = document.getElementById('rubro').value;

  if (searchValue.length > 2 || categoryValue !== '') {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
      if (xhr.readyState === XMLHttpRequest.DONE) {
        if (xhr.status === 200) {
          var results = JSON.parse(xhr.responseText);
          showResults(results);
        } else {
          console.error('Error: ' + xhr.status);
        }
      }
    };
    xhr.open('GET', '../PHP/search.php?q=' + encodeURIComponent(searchValue) + '&category=' + encodeURIComponent(categoryValue), true);
    xhr.send();
  } else {
    clearResults();
  }
}

function showResults(results) {
  var resultsTable = document.getElementById('results');
  resultsTable.innerHTML = '';

  // Crear encabezados de tabla
  var tableHead = document.createElement('thead');
  var headerRow = document.createElement('tr');
  var headers = ['Seleccionar', 'Nombre', 'Descripción', 'Precio taller', 'Precio visita', 'Rubro'];

  headers.forEach(function(headerText) {
    var headerCell = document.createElement('th');
    headerCell.textContent = headerText;
    headerRow.appendChild(headerCell);
  });

  tableHead.appendChild(headerRow);
  resultsTable.appendChild(tableHead);

  // Crear filas de datos
  var tableBody = document.createElement('tbody');

  results.forEach(function(result) {
    var row = document.createElement('tr');

    var checkboxCell = document.createElement('td');
    var checkbox = document.createElement('input');
    checkbox.type = 'checkbox';
    

    // Agrega un evento 'change' a las casillas de verificación
    checkbox.addEventListener('change', function() {
      if (checkbox.checked) {
        selectedRows.push(result);
      } else {
        // Remueve la selección solo si la casilla está desmarcada
        var index = selectedRows.findIndex(function(row) {
          return row.nombre === result.nombre;
        });
        if (index !== -1) {
          selectedRows.splice(index, 1);
        }
      }

      // Guarda las selecciones en el almacenamiento local
      const dataToSave = {
        selectedRows: selectedRows,
        chk_mostrar: chk_mostrar
      };
      localStorage.setItem('myData', JSON.stringify(dataToSave));
    });

    // Marcar la casilla si la fila está seleccionada
    if (selectedRows.some(row => row.nombre === result.nombre)) {
      checkbox.checked = true;
    }

    checkboxCell.appendChild(checkbox);
    row.appendChild(checkboxCell);

    // Crear celdas de datos
    var nombreCell = document.createElement('td');
    nombreCell.textContent = result.nombre;
    row.appendChild(nombreCell);

    var descripcionCell = document.createElement('td');
    descripcionCell.textContent = result.descripcion;
    row.appendChild(descripcionCell);

    var precioTallerCell = document.createElement('td');
    precioTallerCell.textContent = (result.precio_taller * chk_mostrar).toFixed(2);
    row.appendChild(precioTallerCell);

    var precioVisitaCell = document.createElement('td');
    precioVisitaCell.textContent = (result.precio_on_site * chk_mostrar).toFixed(2);
    row.appendChild(precioVisitaCell);

    var rubroCell = document.createElement('td');
    rubroCell.textContent = result.rubro;
    row.appendChild(rubroCell);

    tableBody.appendChild(row);
  });

  resultsTable.appendChild(tableBody);
}

// No permite ingresar caracteres especiales
document.getElementById("searchInput").addEventListener("input", (e) => {
  let value = e.target.value;
  e.target.value = value.replace(/[^A-Za-z]/g, "");
});