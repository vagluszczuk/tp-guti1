function cargar_pagina() {
  verificar_cookies();
  reiniciar_pagina();
}

function verificar_cookies() {
  var xhr = new XMLHttpRequest();
  xhr.open("GET", "../PHP/verificar_sesion.php", true);
  xhr.onreadystatechange = function() {
    if (xhr.readyState === 4 && xhr.status === 200) {
      // Verificar la respuesta del script PHP
      var response = xhr.responseText;
      if (response === "La sesión existe") {
        console.log("La sesión existe");
        // Realizar las operaciones que necesites aquí
      } else {
        console.log("La sesión no existe");
        // Redirigir al usuario al formulario de inicio de sesión
        window.location.href = "../HTML/login.html";
      }
    }
  };
  xhr.send();
}

function reiniciar_pagina() {
  setTimeout(function() {
    location.reload();
  }, 1200000);
}


 
  // Capturar el evento de envío del formulario
  $("#form_carga").submit(function(event) {
    event.preventDefault(); // Evitar el envío del formulario

    // Obtener los datos del formulario
    var formData = $(this).serialize();

    // Agregar 'compra' a los datos del formulario
    formData += "&compra=" + encodeURIComponent(compra);

    // Enviar los datos al servidor mediante AJAX
    $.ajax({
      url: "../PHP/cargarBD.php",
      type: "POST",
      data: formData,
      success: function(response) {
        console.log(response);
        alert("Datos cargados correctamente");

        $("#form_carga")[0].reset();
      },
      error: function(xhr, status, error) {
        // Manejar el error
      }
    });
  });


// Llamar a la función llenarDesplegable cuando el documento esté listo
$(document).ready(function() {
  llenarDesplegable();
});




let compra;

fetch('https://www.dolarsi.com/api/api.php?type=dolar')
.then(response => response.json())
.then(data => {
  const blueObj = data.find(item => item.casa.nombre === "Blue");
  compra = blueObj.casa.compra;
  compra = compra.replace(",", ".");
})
.catch(error => {
  console.error('Error al obtener el archivo JSON:', error);
}); 

// Función para obtener los datos de la base de datos y llenar el desplegable
   function llenarDesplegable() {
    fetch('../PHP/mostrar_lista_rubro.php')
        .then(response => response.json())
        .then(data => {
            var desplegable = document.getElementById('rubro');
            data.forEach(dato => {
                var option = document.createElement('option');
                option.value = dato.rubros;
                option.text = dato.rubros;
                desplegable.appendChild(option);
            });
        })
        .catch(error => console.error('Error:', error));
  }

  function llenarDesplegable1() {
    fetch('../PHP/mostrar_lista_rubro.php')
        .then(response => response.json())
        .then(data => {
            var desplegable = document.getElementById('rubroSelect');
            data.forEach(dato => {
                var option = document.createElement('option');
                option.value = dato.rubros;
                option.text = dato.rubros;
                desplegable.appendChild(option);
            });
        })
        .catch(error => console.error('Error:', error));
  }
  


const input = document.getElementById('desc');
input.addEventListener('input', function() {
  // Llamar a la función postData() cuando cambia el valor del input
  postData();
});

function postData() {
  const form = document.querySelector('#form_datos');
  const formData = new FormData(form);
  const xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {

    console.log('readyState:', this.readyState);
    console.log('status:', this.status);

    if (this.readyState === 4 && this.status === 200) {
      const resultado_busqueda = document.getElementById('resultado_busqueda');
      resultado_busqueda.innerHTML = '';
      const data = JSON.parse(this.responseText);
      if (data.length > 0) {
        const table = document.createElement('table');
        const thead = document.createElement('thead');
        const tbody = document.createElement('tbody');
        const headerRow = document.createElement('tr');
        const headers = ['Codigo', 'Nombre', 'Descripcion', 'Precio taller', 'Precio on site', 'Rubro'];
        headers.forEach(headerText => {
          const header = document.createElement('th');
          const textNode = document.createTextNode(headerText);
          header.appendChild(textNode);
          headerRow.appendChild(header);
        });
        thead.appendChild(headerRow);
        data.forEach(rowData => {
          const row = document.createElement('tr');
        
          Object.entries(rowData).forEach(([key, value]) => {
            const cell = document.createElement('td');
            if (key === 'id cliente') {
              const textNode = document.createTextNode(value);
              cell.appendChild(textNode);
            } else {
              const span = document.createElement('span');
              span.textContent = value;
              cell.appendChild(span);
            }
            row.appendChild(cell);
          });
        
          const editButtonCell = document.createElement('td');
          const editButton = document.createElement('button');
          editButton.textContent = 'Editar';
          editButton.classList.add('editar');
          editButton.addEventListener('click', function() {
            const rowCells = this.parentNode.parentNode.childNodes;
        
            if (this.textContent === 'Editar') {
              for (let i = 1; i < 6; i++) {
                if (headers[i] === 'rubro') {
                  const cellContent = rowCells[i].querySelector('span').textContent;
                  const selectRubro = createRubroSelect(); // Función para crear el select con opciones
                  selectRubro.value = cellContent;
                  rowCells[i].innerHTML = '';
                  rowCells[i].appendChild(selectRubro);
                } else {
                  const cellContent = rowCells[i].querySelector('span').textContent;
                  rowCells[i].innerHTML = `<span contenteditable="true">${cellContent}</span>`;
                }
              }
              this.textContent = 'Guardar';
            } else {
              const updatedRowData = {}; // Objeto para almacenar los datos de la fila actual
              for (let i = 0; i < rowCells.length-1; i++) {
                const headerText = headers[i];
                const cellContent = rowCells[i].querySelector('span')?.textContent || '';
                if (headerText === 'rubro') {
                  const selectValue = rowCells[i].querySelector('select')?.value || '';
                  updatedRowData[headerText] = selectValue;
                } else {
                  updatedRowData[headerText] = cellContent;
                }
              }
        
              // Llamar al archivo PHP "editar_clientes.php" y enviar los datos actualizados de la fila
              const xhrEdit = new XMLHttpRequest();
              xhrEdit.onreadystatechange = function() {
                if (this.readyState === 4 && this.status === 200) {
                  // Realizar cualquier otra acción necesaria después de guardar los datos
        
                  // Volver a llamar a postData() para actualizar los resultados
                  postData();
                }
              };
              console.log('Datos a enviar al servidor:', updatedRowData);
        
              xhrEdit.open('POST', '../PHP/editar_servicios.php', true);
              xhrEdit.setRequestHeader('Content-Type', 'application/json');
              xhrEdit.send(JSON.stringify(updatedRowData)); // Enviar solo los datos de la fila actual
              
              this.textContent = 'Editar';
            }
          });
        
          editButtonCell.appendChild(editButton);
          row.appendChild(editButtonCell);
          tbody.appendChild(row);
        });
        
        table.appendChild(thead);
        table.appendChild(tbody);
        resultado_busqueda.appendChild(table);
      } else {
        // resultado_busqueda.textContent = 'No se encontraron resultados para la fecha seleccionada.';
      }
    }
  };
  xhr.open('POST', form.action, true);
  xhr.send(formData);
}


// Obtener referencia al checkbox
var CheckboxSD = document.getElementById("switch-SD");

// Agregar evento de cambio al checkbox
CheckboxSD.addEventListener("change", function() {
  var th = document.getElementById("SD");
  if (CheckboxSD.checked) {
    th.textContent = "Descripción";
  } else {
    th.textContent = "Servicio";
  }
});

function createRubroSelect() {
  const select = document.createElement('select');
  select.id = 'rubroSelect'; // Asigna un ID al select para identificarlo fácilmente
  llenarDesplegable1(); // Llena las opciones del select
  
  return select;
}

//JS del Replit
$(document).ready(function() {
  $("#toggle-agregar-servicio").click(function() {
    var seccionCambiarPrecio = $("#section-content");
    seccionCambiarPrecio.slideToggle();
  });
});
  
  $(document).ready(function() {
        $("#toggle-modificar-precio").click(function() {
          var seccionCambiarPrecio = $("#seccion-modificar-precio");
          seccionCambiarPrecio.slideToggle();
        });
      });
  
      $(document).ready(function() {
        $("#toggle-cambiar-precio").click(function() {
          var seccionCambiarPrecio = $("#seccion-cambiar-precio");
          seccionCambiarPrecio.slideToggle();
        });
      });
  
  $(document).ready(function() {
    $("#switch-SD").change(function() {
      var descripcionInput = $("#seccion-cambiar-precio input[name='descripcion']");
      if ($(this).prop("checked")) {
        descripcionInput.attr("placeholder", "Descripción");
      } else {
        descripcionInput.attr("placeholder", "Servicio");
      }
    });
  });
 
  $(document).ready(function() {
    // Validar el campo "servicio" para permitir letras, espacios y tildes
    $("#servicio").on("input", function(e) {
        let value = $(this).val();
        if (/[^A-Za-záéíóúÁÉÍÓÚüÜ\s]/.test(value)) {
            alert("No se permiten caracteres especiales en el formulario.");
            $(this).val(value.replace(/[^A-Za-záéíóúÁÉÍÓÚüÜ\s]/g, ""));
        }
    });

    // Validar el campo "precioT" para permitir solo números y punto decimal
    $("#precioT").on("input", function(e) {
        let value = $(this).val();
        if (/[^0-9.]/.test(value)) {
            alert("No se permiten letras en este campo.");
            $(this).val(value.replace(/[^0-9.]/g, ""));
        }
    });

    // Validar el campo "precioO" para permitir solo números y punto decimal
    $("#precioO").on("input", function(e) {
        let value = $(this).val();
        if (/[^0-9.]/.test(value)) {
            alert("No se permiten letras en este campo.");
            $(this).val(value.replace(/[^0-9.]/g, ""));
        }
    });
});

const checkbox1 = document.getElementById('PD1');
const label1 = document.querySelector('.form-check-label[for="pesos1"]');

checkbox1.addEventListener('click', function () {
  if (checkbox1.checked) {
    label1.textContent = 'ARS';
  } else {
    label1.textContent = 'USD';
  }
});

// const checkbox2 = document.getElementById('PD2');
// const label2 = document.querySelector('.form-check-label[for="pesos2"]');

// checkbox2.addEventListener('click', function () {
//   if (checkbox2.checked) {
//     label2.textContent = 'ARS';
//   } else {
//     label2.textContent = 'USD';
//   }
// });

