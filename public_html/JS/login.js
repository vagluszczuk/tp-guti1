  function mostrarOcultarContrasena() {
    var contrasenaInput = document.getElementById("contrasena");
    var boton = document.getElementById("mostrarContrasena");

    if (contrasenaInput.type === "password") {
      contrasenaInput.type = "text";
    } else {
      contrasenaInput.type = "password";
    }
  }

  $(document).ready(function() {
    $('#myCheckbox').change(function() {
      if ($(this).is(':checked')) {
        $(this).next('.checkbox-custom').addClass('checked');
        document.getElementById("contrasena").type = "text";
      } else {
        $(this).next('.checkbox-custom').removeClass('checked');
        document.getElementById("contrasena").type = "password";
      }
    });
  });
  