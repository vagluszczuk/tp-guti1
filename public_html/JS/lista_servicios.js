 
let compra;

fetch('https://www.dolarsi.com/api/api.php?type=dolar')
.then(response => response.json())
.then(data => {
  const blueObj = data.find(item => item.casa.nombre === "Blue");
  compra = blueObj.casa.compra;
  compra = compra.replace(",", ".");
})
.catch(error => {
  console.error('Error al obtener el archivo JSON:', error);
}); 

const savedData = JSON.parse(localStorage.getItem('myData'));

var selectedRows = savedData.selectedRows;
var chk_mostrar = savedData.chk_mostrar;
var tabla = document.getElementById('tablaDatos');
var preciosTotales = {};

// Crear la fila de encabezado
var encabezado = tabla.createTHead().insertRow();
encabezado.insertCell().textContent = 'Nombre';
encabezado.insertCell().textContent = 'Precio taller';
encabezado.insertCell().textContent = 'Precio visita';
encabezado.insertCell().textContent = 'Rubro';


// Crear las filas de datos
selectedRows.forEach(function (rowData) {
    var fila = tabla.insertRow();
    fila.insertCell().textContent = rowData.nombre;

  
    var precioTallerRadioCell = fila.insertCell();
    var precioTallerRadio = document.createElement('input');
    precioTallerRadio.type = 'radio';
    precioTallerRadio.name = 'precioSeleccionado_' + rowData.nombre;
    precioTallerRadio.value = (rowData.precio_taller * chk_mostrar).toFixed(2);
    precioTallerRadio.addEventListener('change', function () {
        preciosTotales[rowData.nombre] = parseFloat((rowData.precio_taller * chk_mostrar).toFixed(2));
        actualizarTotal();
    });
    precioTallerRadioCell.appendChild(precioTallerRadio); // Radio button primero

    var precioVisitaRadioCell = fila.insertCell();
    var precioVisitaRadio = document.createElement('input');
    precioVisitaRadio.type = 'radio';
    precioVisitaRadio.name = 'precioSeleccionado_' + rowData.nombre;
    precioVisitaRadio.value = (rowData.precio_on_site * chk_mostrar).toFixed(2);
    precioVisitaRadio.addEventListener('change', function () {
        preciosTotales[rowData.nombre] = parseFloat((rowData.precio_on_site * chk_mostrar).toFixed(2));
        actualizarTotal();
    });
    precioVisitaRadioCell.appendChild(precioVisitaRadio); // Radio button primero

    // Agregar precios después de los radio buttons
    precioTallerRadioCell.appendChild(document.createTextNode('$' + (rowData.precio_taller * chk_mostrar).toFixed(2)));
    precioVisitaRadioCell.appendChild(document.createTextNode('$' + (rowData.precio_on_site * chk_mostrar).toFixed(2)));

    fila.insertCell().textContent = rowData.rubro;

    preciosTotales[rowData.nombre] = 0; // Inicializar los precios totales
});

var totalDiv = document.createElement('div');
var totalLabel = document.createElement('div');
var totalPrice = document.createElement('div');

totalLabel.textContent = '';
totalPrice.textContent = '0.00'; // Deberás actualizar esto cuando calcules el precio

// Asigna clases CSS para aplicar estilos
totalLabel.classList.add('total-label');
totalPrice.classList.add('total-price');

totalDiv.appendChild(totalLabel);
totalDiv.appendChild(totalPrice);

// Aplicar estilos CSS a totalDiv
totalDiv.style.marginTop = '10px';
totalDiv.style.flexDirection = 'row'; // Cambia la dirección de flex a columna
totalDiv.style.alignItems = 'center';
totalDiv.style.backgroundColor = 'white';
totalDiv.style.border = '2px solid #008FBC';
totalDiv.style.padding = '5px'; // Agrega espacio interno
totalLabel.textContent = 'Total:';
tabla.parentElement.appendChild(totalDiv);
function actualizarTotal() { 
    var total = Object.values(preciosTotales).reduce(function (acc, currentValue) {
        return acc + currentValue;
    }, 0);
    totalPrice.textContent = total.toFixed(2); // Actualizar el total con 2 decimales

    
}

localStorage.removeItem('selectedRows');
selectedRows = [];



document.getElementById('generarPDF').addEventListener('click', function() {
    var datosPDF = []; // Array para almacenar los datos que se mostrarán en el PDF

    // Iterar a través de las filas de la tabla
    var filas = tabla.rows;
    for (var i = 1; i < filas.length; i++) { // Empezar desde 1 para omitir la fila de encabezado
        var fila = filas[i];
        var nombre = fila.cells[0].textContent; // Obtener el nombre de la primera columna

        // Obtener el valor del radio button seleccionado
        var radioButtons = fila.querySelectorAll('input[type="radio"]');
        var precioSeleccionado = "";
        for (var j = 0; j < radioButtons.length; j++) {
            if (radioButtons[j].checked) {
                precioSeleccionado = radioButtons[j].value;
                break;
            }
        }

        // Agregar los datos al array
        datosPDF.push({
            nombre: nombre,
            precioSeleccionado: precioSeleccionado
        });
    }
   // Ahora puedes utilizar 'datosPDF' para crear el PDF con jsPDF
   var doc = new jsPDF('p', 'mm', 'a4');

   doc.setProperties({
       title: 'Presupuesto',
       author: 'Presupuesto',
       subject: 'Presupuesto',
       keywords: 'Presupuesto'
   });

   doc.setFontSize(16);
   doc.text("Presupuesto", 85, 15, { align: 'center' });

   doc.setFontSize(10);
   doc.text('Fecha: ' + new Date().toLocaleDateString(), 10, 25);
   doc.text('Dolar blue: ' + compra, 158, 25);

   doc.text("___________________________________________________________________________________________", 10, 35);

   var y = 45; // Posición vertical inicial para los datos

   doc.setFontSize(10);
   doc.text('Producto', 20, y);
   doc.text('Precio', 160, y);

   var total = 0;

   datosPDF.forEach(function(data) {
       y += 10;
       doc.text(data.nombre, 25, y);
       doc.text(data.precioSeleccionado, 165, y);

       // Calcular el total
       var precio = parseFloat(data.precioSeleccionado.replace('$', ''));
       total += precio;
   });

   y += 10;

   doc.text("___________________________________________________________________________________________", 10, y);
   y += 10;

   doc.text('Subtotal', 20, y);
   doc.text('$' + total.toFixed(2), 70, y);
   y += 10;

   var iva = total * 0.21;
   doc.text('IVA (21%)', 20, y);
   doc.text('$' + iva.toFixed(2), 70, y);
   y += 10;

   var totaltotal = total + iva;
   doc.setFontType('bold');
   doc.text('Total', 20, y);
   doc.text('$' + totaltotal.toFixed(2), 70, y);

   // Generar el PDF en una variable Blob
   var pdfBlob = doc.output('blob');

   // Crear una URL para el Blob
   var pdfUrl = URL.createObjectURL(pdfBlob);

   // Abrir una nueva ventana o pestaña con el PDF
   window.open(pdfUrl, '_blank');
});

// NO CAMBIEN LO DE ARRIBA PORFAAAAA 😊❤👍


